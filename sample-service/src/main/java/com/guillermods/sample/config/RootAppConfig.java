/**
 * Copyright (C) 2015 Guillermo Díaz Solís.
 * Todos los derechos reservados.
 */
package com.guillermods.sample.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * @author memo
 * @date 12/2/2015
 * 
 */
@Configuration
@ComponentScan(basePackages = {"com.guillermods.sample.service"})
@Import(value = {DataBaseConfig.class})
public class RootAppConfig {
	

}
