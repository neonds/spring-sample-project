/**
 * Copyright (C) 2015 Guillermo Díaz Solís.
 * Todos los derechos reservados.
 */
package com.guillermods.sample.service;

import java.util.List;

import com.guillermods.sample.service.dto.SampleDto;

/**
 * @author memo
 * @date 12/2/2015
 * 
 */
public interface ISampleService {

	/**
	 * 
	 * @param sample
	 * @return
	 */
	public int saveSample(SampleDto sample);
	
	/**
	 * 
	 * @param sample
	 * @return
	 */
	public String updateSample(SampleDto sample);
	
	/**
	 * 
	 * @param sample
	 * @return
	 */
	public String deleteSample(SampleDto sample);
	
	/**
	 * 
	 * @param sample
	 * @return
	 */
	public String findSample(SampleDto sample);
	
	/**
	 * 
	 * @return
	 */
	public List<SampleDto> listSample ();
	
}
