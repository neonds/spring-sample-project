/**
 * Copyright (C) 2015 Guillermo Díaz Solís.
 * Todos los derechos reservados.
 */
package com.guillermods.sample.service.dto;

import java.util.Date;

/**
 * @author memo
 * @date 12/2/2015
 * 
 */
public class SampleDto {
	private String id;
	private String name;
	private String comment;
	private Date created;
	private Date updated;

	public SampleDto() {

	}

	/**
	 * @param id
	 * @param name
	 * @param comment
	 * @param created
	 * @param updated
	 */
	public SampleDto(String id, String name, String comment, Date created,
			Date updated) {
		super();
		this.id = id;
		this.name = name;
		this.comment = comment;
		this.created = created;
		this.updated = updated;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the comment
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * @param comment
	 *            the comment to set
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}

	/**
	 * @return the created
	 */
	public Date getCreated() {
		return created;
	}

	/**
	 * @param created
	 *            the created to set
	 */
	public void setCreated(Date created) {
		this.created = created;
	}

	/**
	 * @return the updated
	 */
	public Date getUpdated() {
		return updated;
	}

	/**
	 * @param updated
	 *            the updated to set
	 */
	public void setUpdated(Date updated) {
		this.updated = updated;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "SampleDto [id=" + id + ", name=" + name + ", comment=" + comment
				+ ", created=" + created + ", updated=" + updated + "]";
	}

}
