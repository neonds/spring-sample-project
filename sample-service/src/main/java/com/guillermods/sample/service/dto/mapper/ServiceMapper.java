/**
 * Copyright (C) 2015 Guillermo Díaz Solís.
 * Todos los derechos reservados.
 */
package com.guillermods.sample.service.dto.mapper;
/**
 * 
 * @author Guillermo
 * @date   2/12/2014, 21:39:05
 *
 * @param <S>
 * @param <T>
 */
public interface ServiceMapper<S, T> {

	/**
	 * 
	 * @param source
	 * @param target
	 * @return
	 */
    T map(S source, T target);

    /**
     * 
     * @param source
     * @return
     */
    T mapToDto(S source);

    /**
     * 
     * @param source
     * @return
     */
    S mapToEntity(T source);

}