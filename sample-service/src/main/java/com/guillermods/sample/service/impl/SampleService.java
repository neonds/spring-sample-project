/**
 * Copyright (C) 2015 Crux Consultores.
 * Todos los derechos reservados.
 */
package com.guillermods.sample.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.guillermods.sample.repository.ISampleDao;
import com.guillermods.sample.repository.entity.Sample;
import com.guillermods.sample.service.ISampleService;
import com.guillermods.sample.service.dto.SampleDto;
import com.guillermods.sample.service.dto.mapper.SampleMapperDto;

/**
 * @author memo
 * @date 12/2/2015
 * 
 */
@Service
public class SampleService implements ISampleService{

	@Autowired
	ISampleDao sampleDao;
	
	@Autowired
	SampleMapperDto sampleMapperDto;
	
	/* (non-Javadoc)
	 * @see com.guillermods.sample.service.ISampleService#saveSample(com.cruxconsultores.sample.service.dto.SampleDto)
	 */
	@Override
	public int saveSample(SampleDto sampl) {
		Sample sample = sampleMapperDto.mapToEntity(sampl);
		int result = sampleDao.save(sample);
		return result;
	}

	/* (non-Javadoc)
	 * @see com.guillermods.sample.service.ISampleService#updateSample(com.cruxconsultores.sample.service.dto.SampleDto)
	 */
	@Override
	public String updateSample(SampleDto sample) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.guillermods.sample.service.ISampleService#deleteSample(com.cruxconsultores.sample.service.dto.SampleDto)
	 */
	@Override
	public String deleteSample(SampleDto sample) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.guillermods.sample.service.ISampleService#findSample(com.cruxconsultores.sample.service.dto.SampleDto)
	 */
	@Override
	public String findSample(SampleDto sample) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.guillermods.sample.service.ISampleService#listSample()
	 */
	@Override
	public List<SampleDto> listSample() {
		// TODO Auto-generated method stub
		return null;
	}

}
