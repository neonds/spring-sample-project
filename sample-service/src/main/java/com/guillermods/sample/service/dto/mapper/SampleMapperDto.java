/**
 * Copyright (C) 2015 Guillermo Díaz Solís.
 * Todos los derechos reservados.
 */
package com.guillermods.sample.service.dto.mapper;

import org.springframework.stereotype.Component;

import com.guillermods.sample.repository.entity.Sample;
import com.guillermods.sample.service.dto.SampleDto;

/**
 * @author memo
 * @date 12/2/2015
 * 
 */
@Component
public class SampleMapperDto implements ServiceMapper<Sample, SampleDto> {

	/* (non-Javadoc)
	 * @see com.cruxconsultores.sample.service.dto.mapper.ServiceMapper#map(java.lang.Object, java.lang.Object)
	 */
	@Override
	public SampleDto map(Sample source, SampleDto target) {
		target.setComment(source.getComment());
		target.setCreated(source.getCreated());
		target.setId(source.getId());
		target.setName(source.getName());
		target.setUpdated(source.getUpdated());
		
		return target;
	}

	/* (non-Javadoc)
	 * @see com.guillermods.sample.service.dto.mapper.ServiceMapper#mapToDto(java.lang.Object)
	 */
	@Override
	public SampleDto mapToDto(Sample source) {
		SampleDto target = new SampleDto();
		target.setComment(source.getComment());
		target.setCreated(source.getCreated());
		target.setId(source.getId());
		target.setName(source.getName());
		target.setUpdated(source.getUpdated());
		return target;
	}

	/* (non-Javadoc)
	 * @see com.guillermods.sample.service.dto.mapper.ServiceMapper#mapToEntity(java.lang.Object)
	 */
	@Override
	public Sample mapToEntity(SampleDto source) {
		Sample target = new Sample();
		target.setComment(source.getComment());
		target.setCreated(source.getCreated());
		target.setId(source.getId());
		target.setName(source.getName());
		target.setUpdated(source.getUpdated());
		return target;
	}


}
