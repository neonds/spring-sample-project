/**
 * Copyright (C) 2015 Guillermo Díaz Solís.
 * Todos los derechos reservados.
 */
package com.guillermods.sample.service.impl;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import com.guillermods.sample.config.RootAppConfigTest;

/**
 * @author memo
 * @date 12/2/2015
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { RootAppConfigTest.class }, loader = AnnotationConfigContextLoader.class)
public abstract class AbstractServiceTest {

}
