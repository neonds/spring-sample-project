/**
 * Copyright (C) 2015 Guillermo Díaz Solís.
 * Todos los derechos reservados.
 */
package com.guillermods.sample.service.impl;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.guillermods.sample.service.ISampleService;
import com.guillermods.sample.service.dto.SampleDto;

/**
 * @author memo
 * @date 12/2/2015
 * 
 */
public class SampleServiceTest extends AbstractServiceTest {

	@Autowired
	ISampleService sampleService;
	
	private SampleDto sample;
	
	/**
	 * Este método valida que la inyección de Sample Service
	 * sea la correcta. Además si pasa esta prueba, significa 
	 * que la base de datos en memoria se ejecuta sin problemas hasta
	 * llegar a el modulo de service
	 */
	@Test
	public void testSampleService(){
		System.out.println("Test for injection");
		assertNotNull(sampleService);
	}
	
	@Before
	public void setUp(){
		sample = new SampleDto();
		sample.setComment("Service test");
		sample.setCreated(new Date());
		sample.setId("1235");
		sample.setName("Sample");
		sample.setUpdated(new Date());
	}
	
	/**
	 * Test method for {@link com.guillermods.sample.service.impl.SampleService#saveSample(com.guillermods.sample.service.dto.SampleDto)}.
	 */
	@Test
	public void testSaveSample() {
		System.out.println("SampleServiceTest.testSaveSample: "+sample);
		int expected = 1;
		int actual = sampleService.saveSample(sample);
		assertEquals(expected, actual);
		
	}


}
