/**
 * Copyright (C) 2015 Guillermo Díaz Solís.
 * Todos los derechos reservados.
 */
package com.guillermods.sample.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * @author memo
 * @date 9/3/2015
 * 
 */
@Configuration
@EnableWebMvc
@ComponentScan(basePackages = {"com.guillermods.sample.web.controller"})
public class WebConfig extends WebMvcConfigurerAdapter{

}
