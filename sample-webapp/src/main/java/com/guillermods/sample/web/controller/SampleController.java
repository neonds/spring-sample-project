/**
 * Copyright (C) 2015 Guillermo Díaz Solís.
 * Todos los derechos reservados.
 */
package com.guillermods.sample.web.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.guillermods.sample.repository.entity.Sample;
import com.guillermods.sample.web.RestApi;

/**
 * @author memo
 * @date 9/3/2015
 * 
 */
@RequestMapping(RestApi.CONTEXT)
@RestController
public class SampleController {

	@RequestMapping(
			value 		= 	RestApi.SAMPLES_URL,
			method		=	RequestMethod.GET,
			produces	=   MediaType.APPLICATION_JSON_VALUE 
	)
	
	public ResponseEntity<List<Sample>> listSamples(){
		
		List<Sample> samples = new ArrayList<Sample>();
		Sample sample = new Sample();
		sample.setComment("Este es el comentario 1");
		sample.setCreated(new Date());
		sample.setId("4000");
		sample.setName("Sample 4");
		sample.setUpdated(new Date());
		
		samples.add(sample);
		samples.add(sample);
		
		return new ResponseEntity<List<Sample>>(samples, HttpStatus.OK);
	}

}
