/**
 * Copyright (C) 2015 Guillermo Díaz Solís.
 * Todos los derechos reservados.
 */
package com.guillermods.sample.web;

/**
 * @author memo
 * @date 9/3/2015
 * 
 */

public class RestApi {

	/**
	 * Define el contexto inicial de la api
	 */
	public final static String CONTEXT = "api";
	public final static String SAMPLES_URL = "samples";
	public final static String SAMPLES_BY_ID_URL = SAMPLES_URL + "/{id}";
	
	
	
}

