create table samples(
	id varchar(50),
	name varchar(100) not null,
	comment varchar(100) default '',
	created	date,
	updated date,
	constraint samples_pk 
	primary key (id)
);