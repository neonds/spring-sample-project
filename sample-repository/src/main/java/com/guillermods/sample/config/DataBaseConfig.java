/**
 * Copyright (C) 2015 Guillermo Díaz Solís.
 * Todos los derechos reservados.
 */
package com.guillermods.sample.config;

import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.lookup.JndiDataSourceLookup;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author Guillermo Díaz Solís
 * @date 12/2/2015
 * 
 * Configuración de acceso a datos
 */
@Configuration
@ComponentScan("com.guillermods.sample.repository")
@EnableTransactionManagement
public class DataBaseConfig {

	/**
	 * JNDI debe ser configurado en el servidor respectivo
	 */
	private static final String RESOURCE_JNDI = "jdbc-connection";
	
	/**
	 * @return DataSource
	 * @throws SQLException
	 */
	@Bean
	public DataSource dataSource() throws SQLException {
		final JndiDataSourceLookup dsLookup = new JndiDataSourceLookup();
		dsLookup.setResourceRef(true);
		DataSource dataSource = dsLookup.getDataSource(RESOURCE_JNDI);
		return dataSource;
	}
	
	/**
	 * @param dataSource
	 * @return DataSourceTransactionManager
	 */
	@Bean
	public DataSourceTransactionManager getDataSourceTransactionManager(
			final DataSource dataSource) {
		DataSourceTransactionManager transManager = new DataSourceTransactionManager();
		transManager.setDataSource(dataSource);
		return transManager;

	}
}
