/**
 * Copyright (C) 2015 Guillermo Díaz Solís.
 * Todos los derechos reservados.
 */
package com.guillermods.sample.repository.entity.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.guillermods.sample.repository.entity.Sample;

/**
 * @author memo
 * @date 12/2/2015
 * 
 */
public class SampleMapper implements RowMapper<Sample>{

	/**
	 * columnas de la tabla, deben ser
	 * publicas y estaticas, esto permite accesar al 
	 * rowMapper y conocer las columnas desde cualquier
	 * parte del proyecto.
	 */
	
	/**
	 *  Columna updated
	 */
	public static final String UPDATED = "updated";

	/**
	 * Columna created
	 */
	public static final String CREATED = "created";

	/**
	 * Columna name
	 */
	public static final String NAME = "name";

	/**
	 * Columna id
	 */
	public static final String ID = "id";

	/**
	 * Columan comments
	 */
	public static final String COMMENTS = "comments";

	/* (non-Javadoc)
	 * @see org.springframework.jdbc.core.RowMapper#mapRow(java.sql.ResultSet, int)
	 */
	@Override
	public Sample mapRow(ResultSet rs, int row) throws SQLException {
		Sample sample = new Sample();
		sample.setComment(rs.getString(COMMENTS));
		sample.setId(rs.getString(ID));
		sample.setName(NAME);
		sample.setCreated(rs.getDate(CREATED));
		sample.setUpdated(rs.getDate(UPDATED));
		return sample;
	}

}
