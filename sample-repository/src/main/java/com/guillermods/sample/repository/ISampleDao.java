/**
 * Copyright (C) 2015 Guillermo Díaz Solís.
 * Todos los derechos reservados.
 */
package com.guillermods.sample.repository;

import java.util.List;

import com.guillermods.sample.repository.entity.Sample;

/**
 * @author memo
 * @date 12/2/2015
 * 
 * Interface de ejemplo
 */
public interface ISampleDao {


	/**
	 * Inserta en la tabla
	 * @param sample
	 * @return El id del objeto insertado
	 */
	public int save(Sample sample);
	
	
	/**
	 * Actualiza el objeto en la tabla
	 * @param sample
	 * @return El id del objeto actualizado
	 */
	public String update(Sample sample);
	
	/**
	 * Elimina el objeto de la tabla
	 * @param sample
	 * @return El id de objeto eliminado
	 */
	public String delete(Sample sample);
	
	
	/**
	 * Obtiene por id, el objeto de la tabla
	 * @param id
	 * @return El objeto encontrado
	 */
	public Sample find(String id);
	
	/**
	 * Lista todos los objetos de la tabla
	 * @return Una lista de objetos
	 */
	public List<Sample> list();
}
