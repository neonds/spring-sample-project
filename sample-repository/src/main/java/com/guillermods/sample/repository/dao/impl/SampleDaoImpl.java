/**
 * Copyright (C) 2015 Guillermo Díaz Solís.
 * Todos los derechos reservados.
 */
package com.guillermods.sample.repository.dao.impl;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.guillermods.sample.repository.ISampleDao;
import com.guillermods.sample.repository.entity.Sample;

/**
 * @author memo
 * @date 12/2/2015
 * 
 */

@Repository
public class SampleDaoImpl implements ISampleDao {

	@Autowired
	private DataSource dataSource;

	private JdbcTemplate jdbcTemplate;

	private static final String SQL_INSERT = "insert into samples(id, name, comment, created, updated) values (?, ?, ?, ?, ?)";

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.guillermods.sample.repository.ISampleDao#save(com.cruxconsultores
	 * .sample.repository.entity.Sample)
	 */
	@Override
	public int save(Sample sample) {
		jdbcTemplate = new JdbcTemplate(dataSource);
		int rows = jdbcTemplate.update(SQL_INSERT, sample.getId(),
				sample.getName(), sample.getComment(), sample.getCreated(),
				sample.getUpdated());

		return rows;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.guillermods.sample.repository.ISampleDao#update(com.cruxconsultores
	 * .sample.repository.entity.Sample)
	 */
	@Override
	public String update(Sample sample) {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.guillermods.sample.repository.ISampleDao#delete(com.cruxconsultores
	 * .sample.repository.entity.Sample)
	 */
	@Override
	public String delete(Sample sample) {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.guillermods.sample.repository.ISampleDao#find(java.lang.String)
	 */
	@Override
	public Sample find(String id) {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.guillermods.sample.repository.ISampleDao#list()
	 */
	@Override
	public List<Sample> list() {
		// TODO Auto-generated method stub
		return null;
	}

}
