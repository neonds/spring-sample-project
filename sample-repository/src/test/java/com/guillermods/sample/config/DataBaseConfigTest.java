/**
 * Copyright (C) 2015 Guillermo Díaz Solís.
 * Todos los derechos reservados.
 */
package com.guillermods.sample.config;

import javax.sql.DataSource;

import org.flywaydb.core.Flyway;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

/**
 * @author memo
 * @date 12/2/2015
 * 
 * Esta clase realiza una configuración de base de datos en memoria
 * Es ejecutada solamente durante la fase de pruebas de maven.
 * 
 * Todos los métodos de integración deben ser testeados apartir de esta
 * clase, nunca de la configuración real de la base de datos, ya que se
 * considera como una dependencia,adicionalmente los "integration test" son 
 * más rápidos de ejecutar.
 * 
 */
@Configuration
@ComponentScan(basePackages = {"com.guillermods.sample.repository"})
public class DataBaseConfigTest {
	

	private static String USER = "sa";
	private static String URL = "jdbc:h2:mem:migrationtestdb;"
			+ "DB_CLOSE_DELAY=5;TRACE_LEVEL_SYSTEM_OUT=1";
	
	/**
	 * 
	 * @return DataSource
	 */
	@Bean
	public DataSource dataSource(){
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setUsername(USER);
        dataSource.setPassword("");
        dataSource.setUrl(URL);
        return dataSource;
	}
	
	/**
	 * 
	 * @return Flyway
	 */
	@Bean
	public Flyway flyway(){
		Flyway flyway = new Flyway();
		flyway.setLocations("classpath:db/migration");
		flyway.setDataSource(this.dataSource());
		flyway.migrate();
		return flyway;
	}

}