/**
 * Copyright (C) 2015 Guillermo Díaz Solís.
 * Todos los derechos reservados.
 */
package com.guillermods.sample.repository;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.transaction.annotation.Transactional;

import com.guillermods.sample.config.DataBaseConfigTest;


/**
 * @author memo
 * @date 12/2/2015
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { DataBaseConfigTest.class }, loader = AnnotationConfigContextLoader.class)
@Transactional
public abstract class AbstractRepositoryTest {

}
