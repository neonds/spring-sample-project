/**
 * Copyright (C) 2015 Guillermo Díaz Solís.
 * Todos los derechos reservados.
 */
package com.guillermods.sample.repository.dao.impl;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.guillermods.sample.repository.AbstractRepositoryTest;
import com.guillermods.sample.repository.ISampleDao;
import com.guillermods.sample.repository.entity.Sample;

/**
 * @author memo
 * @date 12/2/2015
 * 
 */
public class SampleDaoImplTest extends AbstractRepositoryTest {

	@Autowired
	ISampleDao sampleDao;

	private Sample sample;

	@Before
	public void setUp() {
		sample = new Sample("1", "Prueba", "Objeto prueba", new Date(),
				new Date());

	}
	
	
	@Test
	public void testSampleDao(){
		System.out.println("SampleDao Injected!");
		assertNotNull(sampleDao);
		
	}

	/**
	 * Test method for
	 * {@link com.guillermods.sample.repository.dao.impl.SampleDaoImpl#save(com.guillermods.sample.repository.entity.Sample)}
	 * .
	 */
	@Test
	public void testSave() {
		System.out.println("SampleDaoImplTest.testSave " + sample);
		assertTrue(true);
	}

	/**
	 * Test method for
	 * {@link com.guillermods.sample.repository.dao.impl.SampleDaoImpl#update(com.guillermods.sample.repository.entity.Sample)}
	 * .
	 */
	@Test
	public void testUpdate() {
		System.out.println("SampleDaoImplTest.testUpdate " + sample);
		assertTrue(true);
	}

	/**
	 * Test method for
	 * {@link com.guillermods.sample.repository.dao.impl.SampleDaoImpl#delete(com.guillermods.sample.repository.entity.Sample)}
	 * .
	 */
	@Test
	public void testDelete() {
		System.out.println("SampleDaoImplTest.testDelete " + sample);
		assertTrue(true);
	}

	/**
	 * Test method for
	 * {@link com.guillermods.sample.repository.dao.impl.SampleDaoImpl#find(java.lang.String)}
	 * .
	 */
	@Test
	public void testFind() {
		System.out.println("SampleDaoImplTest.testFind " + sample);
		assertTrue(true);
	}

	/**
	 * Test method for
	 * {@link com.guillermods.sample.repository.dao.impl.SampleDaoImpl#list()}
	 * .
	 */
	@Test
	public void testList() {	
		System.out.println("SampleDaoImplTest.testList size " +  10);
		assertTrue(true);
	}

}
