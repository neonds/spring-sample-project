/**
 * Copyright (C) 2015 Guillermo Díaz Solís.
 * Todos los derechos reservados.
 */
package com.guillermods.sample.repository;

import static org.junit.Assert.*;

import javax.sql.DataSource;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author memo
 * @date 12/2/2015
 * Esta Clase prueba que el datasource sea correctamente inyectado
 */
public class DataBaseConfiguredTest extends AbstractRepositoryTest{
	
	@Autowired
	DataSource dataSource;
	
	@Test
	public void testDataSource() {
		assertNotNull(dataSource);
	}

}
